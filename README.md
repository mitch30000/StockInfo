StockInfo
=========

Java library that can get information about different stocks based on their symbol.  This information is obtained from Yahoo Finance via YQL, but has been designed to allow tie-in to other financial providers if needed.

NOTE as of 1-3-2019 YQL has been retired, therefore this library is in an unstable state until a new provider/implementation is added.


### Usage
This library is designed with the ability to call and retrieve stock infromation from multiple providers (currently we use Yahoo Finance via YQL).  The providers are called via implementations of [StockFinder](src/main/java/com/mitchellbdunn/stockinfo/control/StockFinder.java) which take the JSON retrieved from the provider and convert it via [a stock translator](src/main/java/com/mitchellbdunn/stockinfo/control/translator/current/CurrentStockTranslator.java).  This translator takes the JSON from the provider and converts it into an obejct that extends our base [Stock model](src/main/java/com/mitchellbdunn/stockinfo/model/Stock.java) which can be used by the program/application calling this library.  Currently there are implementations for [CurrentStock](src/main/java/com/mitchellbdunn/stockinfo/model/current/CurrentStock.java) which shows stock information at the point of time of the API call, and [HistoricStock](src/main/java/com/mitchellbdunn/stockinfo/model/historic/HistoricStock.java) which shows information about stock pricing in the past.


### Installation
This project uses Maven, so all you have to do to build is to run `mvn clean install` at the base directory where pom.xml is located.  This builds the library and places it into your local m2 repository where you can use it as a dependency in your project as follows:
```
<dependency>
    <groupId>mitch30000</groupId>  
    <artifactId>StockInfo</artifactId>  
    <version>1.0-SNAPSHOT</version>  
</dependency>  
```

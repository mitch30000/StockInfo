package com.mitchellbdunn.stockinfo.control.translator.current;

import com.mitchellbdunn.stockinfo.exception.StockException;
import com.mitchellbdunn.stockinfo.model.current.CurrentStock;
import com.mitchellbdunn.stockinfo.model.property.Properties;
import com.mitchellbdunn.stockinfo.model.property.PropertyConstants;
import java.lang.reflect.InvocationTargetException;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.node.NullNode;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author Mitch
 */
public final class CurrentStockTranslator {

    private final DateTimeFormatter formatter;
    private static final Logger LOGGER = Logger.getLogger(CurrentStockTranslator.class);

    public CurrentStockTranslator() {
        formatter = DateTimeFormat.forPattern("MMM  dd");
    }

    public CurrentStock translateJsonToStock(JsonNode result) throws StockException {
        if (result instanceof NullNode) {
            throw new StockException("No results were returned from the service");
        }
        // If this field is not null, then it means the symbol does not exist
        if (!result.path("ErrorIndicationreturnedforsymbolchangedinvalid").isNull()) {
            LOGGER.error("No stock found matching the symbol " + result.path("Symbol").asText());
            return null;
        }
        LOGGER.debug("Parsing information for '" + result.path("Symbol").asText() + "'");
        CurrentStock stock = new CurrentStock();
        for (Properties prop : Properties.values()) {
            JsonNode node = result.path(prop.getJsonName());
            Object value = null;
            switch (prop.getObjectValue()) {
                case PropertyConstants.STRING_VALUE:
                    value = node.asText();
                    break;
                case PropertyConstants.DOUBLE_VALUE:
                    value = node.asDouble();
                    break;
                case PropertyConstants.INT_VALUE:
                    value = node.asInt();
                    break;
                case PropertyConstants.LONG_VALUE:
                    value = node.asLong();
                    break;
                case PropertyConstants.DATE_VALUE:
                    try {
                        value = formatter.parseDateTime(node.asText());
                    } catch (Exception ex) {
                        LOGGER.error("Error parsing datetime", ex);
                    }
                    break;
            }
            try {
                PropertyUtils.setProperty(stock, prop.getObjectName(), value);
            } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException ex) {
                LOGGER.error("Error translating json values into object", ex);
            }
        }
        // If dividend per share is 0, then no dividend
        if (stock.getDividend().getDividendShare() == 0.0) {
            LOGGER.debug("No dividend listed for " + result.path("Symbol").asText());
            stock.setDividend(null);
        }
        return stock;
    }
}

package com.mitchellbdunn.stockinfo.control.historic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import com.mitchellbdunn.stockinfo.control.StockFinder;
import com.mitchellbdunn.stockinfo.exception.StockException;
import com.mitchellbdunn.stockinfo.model.Stock;
import com.mitchellbdunn.stockinfo.model.historic.HistoricStock;
import com.mitchellbdunn.stockinfo.model.historic.PriceHistory;
import org.apache.http.client.utils.URIBuilder;
import org.codehaus.jackson.JsonNode;
import org.joda.time.Interval;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author Mitch
 */
public class HistoricStockFinder extends StockFinder {

    private static final String HISTORIC_QUERY = "select * from yahoo.finance.historicaldata where symbol=\"%s\" and startDate=\"%s\" and endDate=\"%s\"";
    private DateTimeFormatter formatter;
    private URIBuilder builder;

    public HistoricStockFinder() throws StockException {
        try {
            formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
            builder = new URIBuilder();
            builder.setPath("http://query.yahooapis.com/v1/public/yql")
                    .addParameter("format", "json")
                    .addParameter("env", "store://datatables.org/alltableswithkeys");
        } catch (Exception ex) {
            throw new StockException("Error during YQLStockFinder construction", ex);
        }
    }

    private HistoricStock parseToHistoricStock(JsonNode quote) throws StockException {
        HistoricStock stock = new HistoricStock();
        try {
            List<PriceHistory> priceHistory = new ArrayList<>();
            Iterator<JsonNode> iter = quote.getElements();
            while (iter.hasNext()) {
                JsonNode node = iter.next();
                PriceHistory history = new PriceHistory();
                history.setDate(formatter.parseDateTime(node.path("Date").asText()));
                history.setDayLow(node.path("Low").asDouble());
                history.setDayHigh(node.path("High").asDouble());
                history.setOpen(node.path("Open").asDouble());
                history.setClose(node.path("Close").asDouble());
                history.setAdjustedClose(node.path("Adj_Close").asDouble());
                priceHistory.add(history);
            }
            stock.setPriceHistory(priceHistory);
        } catch (Exception ex) {
            throw new StockException("Error during json parsing", ex);
        }
        return stock;
    }

    public Stock findHistoricStock(Interval interval, String symbol) throws StockException {
        String start = interval.getStart().toDateTime().toString(formatter);
        String end = interval.getEnd().toDateTime().toString(formatter);
        builder.setParameter("q", String.format(HISTORIC_QUERY, symbol, start, end));
        JsonNode result = callService(builder).path("query").path("results").path("quote");
        return parseToHistoricStock(result);
    }

    public List<Stock> findHistoricStocks(Interval interval, String... symbols) throws StockException {
        List<Stock> stocks = new ArrayList<>();
        for (String symbol : symbols) {
            stocks.add(findHistoricStock(interval, symbol));
        }
        return stocks;
    }
}

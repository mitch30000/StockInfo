package com.mitchellbdunn.stockinfo.control;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import com.mitchellbdunn.stockinfo.exception.StockException;
import java.io.IOException;
import java.net.URISyntaxException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author Mitch
 */
public abstract class StockFinder {

    protected JsonNode callService(URIBuilder builder) throws StockException {
        String json = "";
        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet get = new HttpGet(builder.build());
            HttpResponse response = client.execute(get);
            if (response.getStatusLine().getStatusCode() != 200) {
                throw new StockException("Status code of " + response.getStatusLine().getStatusCode() + " was returned");
            }
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStreamReader is = new InputStreamReader(entity.getContent());
                BufferedReader reader = new BufferedReader(is);
                json = reader.readLine();
            }
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readTree(json);
        } catch (URISyntaxException | IOException | IllegalStateException ex) {
            throw new StockException("Error during service call", ex);
        }
    }
}

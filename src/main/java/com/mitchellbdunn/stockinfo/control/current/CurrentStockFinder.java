package com.mitchellbdunn.stockinfo.control.current;

import com.mitchellbdunn.stockinfo.control.StockFinder;
import com.mitchellbdunn.stockinfo.control.translator.current.CurrentStockTranslator;
import com.mitchellbdunn.stockinfo.exception.StockException;
import com.mitchellbdunn.stockinfo.model.Stock;
import com.mitchellbdunn.stockinfo.model.current.CurrentStock;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.http.client.utils.URIBuilder;
import org.codehaus.jackson.JsonNode;

/**
 *
 * @author Mitch
 */
public class CurrentStockFinder extends StockFinder {

    private static final String CURRENT_QUERY = "select * from yahoo.finance.quotes where symbol in (%s)";
    private URIBuilder builder;
    private CurrentStockTranslator translator;

    public CurrentStockFinder() throws StockException {
        try {
            builder = new URIBuilder();
            builder.setPath("http://query.yahooapis.com/v1/public/yql")
                    .addParameter("format", "json")
                    .addParameter("env", "store://datatables.org/alltableswithkeys");
            translator = new CurrentStockTranslator();
        } catch (Exception ex) {
            throw new StockException("Error during YQLStockFinder construction", ex);
        }
    }

    public Stock findCurrentStock(String symbol) throws StockException {
        builder.setParameter("q", String.format(CURRENT_QUERY, "\"" + symbol + "\""));
        JsonNode result = callService(builder);
        return parseStock(result);
    }

    public List<Stock> findCurrentStocks(String... symbols) throws StockException {
        if (symbols.length == 0) {
            throw new StockException("Cannot run findCurrentStocks when no stocks have been provided");
        }
        String symbolsQuery = "";
        String seperator = "";
        for (String symbol : symbols) {
            symbolsQuery += seperator + "\"" + symbol + "\"";
            seperator = ",";
        }
        builder.setParameter("q", String.format(CURRENT_QUERY, symbolsQuery));
        JsonNode json = callService(builder);
        return parseStocks(json);
    }

    public Stock updateCurrentStock(Stock stock) throws StockException {
        builder.setParameter("q", String.format(CURRENT_QUERY, "\"" + stock.getSymbol() + "\""));
        JsonNode result = callService(builder);
        return parseStock(result);
    }

    public List<Stock> updateCurrentStocks(List<Stock> stocks) throws StockException {
        if (stocks.isEmpty()) {
            throw new StockException("Cannot run updateCurrentStocks when no stocks have been provided");
        }
        String symbolsQuery = "";
        String seperator = "";
        for (Stock stock : stocks) {
            symbolsQuery += seperator + "\"" + stock.getSymbol() + "\"";
            seperator = ",";
        }
        builder.setParameter("q", String.format(CURRENT_QUERY, symbolsQuery));
        JsonNode json = callService(builder);
        return parseStocks(json);
    }

    private List<Stock> parseStocks(JsonNode json) throws StockException {
        List<Stock> stocks = new ArrayList<>();
        JsonNode results;
        try {
            results = json.path("query").path("results").path("quote");
        } catch (Exception ex) {
            throw new StockException("Error during json parsing", ex);
        }
        Iterator<JsonNode> iter = results.getElements();
        while (iter.hasNext()) {
            JsonNode result = iter.next();
            CurrentStock stock = translator.translateJsonToStock(result);
            if(stock != null) {
                stocks.add(stock);
            }
        }
        return stocks;
    }

    private CurrentStock parseStock(JsonNode json) throws StockException {
        JsonNode result;
        try {
            result = json.path("query").path("results").path("quote");
        } catch (Exception ex) {
            throw new StockException("Error during json parsing", ex);
        }
        return translator.translateJsonToStock(result);
    }

    public void setBuilder(URIBuilder builder) {
        this.builder = builder;
    }
}

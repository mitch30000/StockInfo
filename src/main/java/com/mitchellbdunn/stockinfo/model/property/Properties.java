
package com.mitchellbdunn.stockinfo.model.property;

/**
 *
 * @author Mitch
 */
public enum Properties {
    
    // Stock properties
    SYMBOL("symbol", PropertyConstants.STRING_VALUE, "Symbol"),
    COMPANY_NAME("companyName", PropertyConstants.STRING_VALUE, "Name"),
    EXCHANGE("exchange", PropertyConstants.STRING_VALUE, "StockExchange"),
    CHANGE("change", PropertyConstants.DOUBLE_VALUE, "Change"),
    DAY_LOW("dayLow", PropertyConstants.DOUBLE_VALUE, "DaysLow"),
    DAY_HIGH("dayHigh", PropertyConstants.DOUBLE_VALUE, "DaysHigh"),
    PRICE("price", PropertyConstants.DOUBLE_VALUE, "LastTradePriceOnly"),
    VOLUME("volume", PropertyConstants.LONG_VALUE, "Volume"),
    BID("bid", PropertyConstants.DOUBLE_VALUE, "Bid"),
    ASK("ask", PropertyConstants.DOUBLE_VALUE, "Ask"),
    AVERAGE_DAILY_VOLUME("averageDailyVolume", PropertyConstants.INT_VALUE, "AverageDailyVolume"),
    YEAR_LOW("yearLow", PropertyConstants.DOUBLE_VALUE, "YearLow"),
    YEAR_HIGH("yearHigh", PropertyConstants.DOUBLE_VALUE, "YearHigh"),
    PE_RATIO("peRatio", PropertyConstants.DOUBLE_VALUE, "PERatio"),
    PEG_RATIO("pegRatio", PropertyConstants.DOUBLE_VALUE, "PEGRatio"),
    // Dividend properties
    DIVIDEND_SHARE("dividend.dividendShare", PropertyConstants.DOUBLE_VALUE, "DividendShare"),
    EX_DIVIDEND_DATE("dividend.exDividendDate", PropertyConstants.DATE_VALUE, "ExDividendDate"),
    DIVIDEND_PAY_DATE("dividend.dividendPayDate", PropertyConstants.DATE_VALUE, "DividendPayDate"),
    DIVIDEND_YIELD("dividend.dividendYield", PropertyConstants.DOUBLE_VALUE, "DividendYield");
    
    private final String objectName;
    private final String objectValue;
    private final String jsonName;
    
    Properties(String objectName, String objectValue, String jsonName) {
        this.objectName = objectName;
        this.objectValue = objectValue;
        this.jsonName = jsonName;
    }

    public String getObjectName() {
        return objectName;
    }

    public String getObjectValue() {
        return objectValue;
    }

    public String getJsonName() {
        return jsonName;
    }
}


package com.mitchellbdunn.stockinfo.model.property;

/**
 *
 * @author Mitch
 */
public final class PropertyConstants {
    
    // Property values
    public static final String STRING_VALUE = "string";
    public static final String INT_VALUE = "int";
    public static final String DOUBLE_VALUE = "double";
    public static final String DATE_VALUE = "date";
    public static final String LONG_VALUE = "long";
    
    private PropertyConstants() {
        
    }
}

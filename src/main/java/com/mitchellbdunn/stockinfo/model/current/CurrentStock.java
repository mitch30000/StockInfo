package com.mitchellbdunn.stockinfo.model.current;

import com.mitchellbdunn.stockinfo.model.Dividend;
import com.mitchellbdunn.stockinfo.model.Stock;

/**
 *
 * @author Mitch
 */
public class CurrentStock extends Stock {
    
    private Long volume;
    private Double change;
    private Double dayLow;
    private Double dayHigh;
    private Double price;
    private Double bid;
    private Double ask;
    private Integer averageDailyVolume;
    private Double yearLow;
    private Double yearHigh;
    private Double peRatio;
    private Double pegRatio;
    private Dividend dividend;

    public CurrentStock() {
        this.dividend = new Dividend();
    }
    
    /**
     * @return the volume
     */
    public Long getVolume() {
        return volume;
    }

    /**
     * @param volume the volume to set
     */
    public void setVolume(Long volume) {
        this.volume = volume;
    }

    /**
     * @return the change
     */
    public Double getChange() {
        return change;
    }

    /**
     * @param change the change to set
     */
    public void setChange(Double change) {
        this.change = change;
    }

    /**
     * @return the low
     */
    public Double getDayLow() {
        return dayLow;
    }

    /**
     * @param dayLow the low to set
     */
    public void setDayLow(Double dayLow) {
        this.dayLow = dayLow;
    }

    /**
     * @return the high
     */
    public Double getDayHigh() {
        return dayHigh;
    }

    /**
     * @param dayHigh the high to set
     */
    public void setDayHigh(Double dayHigh) {
        this.dayHigh = dayHigh;
    }

    /**
     * @return the price
     */
    public Double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(Double price) {
        this.price = price;
    }

    /**
     * @return the bid
     */
    public Double getBid() {
        return bid;
    }

    /**
     * @param bid the bid to set
     */
    public void setBid(Double bid) {
        this.bid = bid;
    }

    /**
     * @return the ask
     */
    public Double getAsk() {
        return ask;
    }

    /**
     * @param ask the ask to set
     */
    public void setAsk(Double ask) {
        this.ask = ask;
    }

    /**
     * @return the averageDailyVolume
     */
    public Integer getAverageDailyVolume() {
        return averageDailyVolume;
    }

    /**
     * @param averageDailyVolume the averageDailyVolume to set
     */
    public void setAverageDailyVolume(Integer averageDailyVolume) {
        this.averageDailyVolume = averageDailyVolume;
    }

    /**
     * @return the yearLow
     */
    public Double getYearLow() {
        return yearLow;
    }

    /**
     * @param yearLow the yearLow to set
     */
    public void setYearLow(Double yearLow) {
        this.yearLow = yearLow;
    }

    /**
     * @return the yearHigh
     */
    public Double getYearHigh() {
        return yearHigh;
    }

    /**
     * @param yearHigh the yearHigh to set
     */
    public void setYearHigh(Double yearHigh) {
        this.yearHigh = yearHigh;
    }

    /**
     * @return the peRatio
     */
    public Double getPeRatio() {
        return peRatio;
    }

    /**
     * @param peRatio the peRatio to set
     */
    public void setPeRatio(Double peRatio) {
        this.peRatio = peRatio;
    }

    /**
     * @return the pegRatio
     */
    public Double getPegRatio() {
        return pegRatio;
    }

    /**
     * @param pegRatio the pegRatio to set
     */
    public void setPegRatio(Double pegRatio) {
        this.pegRatio = pegRatio;
    }

    /**
     * @return the dividend
     */
    public Dividend getDividend() {
        return dividend;
    }

    /**
     * @param dividend the dividend to set
     */
    public void setDividend(Dividend dividend) {
        this.dividend = dividend;
    }

    public boolean hasDividend() {
        return dividend != null;
    }
}

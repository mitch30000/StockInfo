package com.mitchellbdunn.stockinfo.model;

import org.joda.time.DateTime;

/**
 *
 * @author Mitch
 */
public class Dividend {
    
    private Double dividendShare;
    private DateTime exDividendDate;
    private DateTime dividendPayDate;
    private Double dividendYield;

    /**
     * @return the dividendShare
     */
    public Double getDividendShare() {
        return dividendShare;
    }

    /**
     * @param dividendShare the dividendShare to set
     */
    public void setDividendShare(Double dividendShare) {
        this.dividendShare = dividendShare;
    }

    /**
     * @return the exDividendDate
     */
    public DateTime getExDividendDate() {
        return exDividendDate;
    }

    /**
     * @param exDividendDate the exDividendDate to set
     */
    public void setExDividendDate(DateTime exDividendDate) {
        this.exDividendDate = exDividendDate;
    }

    /**
     * @return the dividendPayDate
     */
    public DateTime getDividendPayDate() {
        return dividendPayDate;
    }

    /**
     * @param dividendPayDate the dividendPayDate to set
     */
    public void setDividendPayDate(DateTime dividendPayDate) {
        this.dividendPayDate = dividendPayDate;
    }

    /**
     * @return the dividendYield
     */
    public Double getDividendYield() {
        return dividendYield;
    }

    /**
     * @param dividendYield the dividendYield to set
     */
    public void setDividendYield(Double dividendYield) {
        this.dividendYield = dividendYield;
    }
}

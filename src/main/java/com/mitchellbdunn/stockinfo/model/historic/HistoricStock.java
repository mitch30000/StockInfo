package com.mitchellbdunn.stockinfo.model.historic;

import java.util.List;
import com.mitchellbdunn.stockinfo.model.Stock;

/**
 *
 * @author Mitch
 */
public class HistoricStock extends Stock {
    
    private List<PriceHistory> priceHistory; 

    /**
     * @return the priceHistory
     */
    public List<PriceHistory> getPriceHistory() {
        return priceHistory;
    }

    /**
     * @param priceHistory the priceHistory to set
     */
    public void setPriceHistory(List<PriceHistory> priceHistory) {
        this.priceHistory = priceHistory;
    }
}

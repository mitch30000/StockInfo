package com.mitchellbdunn.stockinfo.model.historic;

import org.joda.time.DateTime;

/**
 *
 * @author Mitch
 */
public class PriceHistory {
    
    private Double open;
    private Double close;
    private Double adjustedClose;
    private Double dayHigh;
    private Double dayLow;
    private DateTime date;

    /**
     * @return the open
     */
    public Double getOpen() {
        return open;
    }

    /**
     * @param open the open to set
     */
    public void setOpen(Double open) {
        this.open = open;
    }

    /**
     * @return the close
     */
    public Double getClose() {
        return close;
    }

    /**
     * @param close the close to set
     */
    public void setClose(Double close) {
        this.close = close;
    }

    /**
     * @return the adjustedClose
     */
    public Double getAdjustedClose() {
        return adjustedClose;
    }

    /**
     * @param adjustedClose the adjustedClose to set
     */
    public void setAdjustedClose(Double adjustedClose) {
        this.adjustedClose = adjustedClose;
    }
    
    /**
     * @return the high
     */
    public Double getDayHigh() {
        return dayHigh;
    }

    /**
     * @param dayHigh the high to set
     */
    public void setDayHigh(Double dayHigh) {
        this.dayHigh = dayHigh;
    }

    /**
     * @return the low
     */
    public Double getDayLow() {
        return dayLow;
    }

    /**
     * @param daysLow the low to set
     */
    public void setDayLow(Double daysLow) {
        this.dayLow = daysLow;
    }

    /**
     * @return the date
     */
    public DateTime getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(DateTime date) {
        this.date = date;
    }   
}

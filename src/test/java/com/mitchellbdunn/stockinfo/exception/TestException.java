package com.mitchellbdunn.stockinfo.exception;

import java.io.IOException;
import junit.framework.TestCase;

/**
 *
 * @author Mitch
 */
public class TestException extends TestCase {
    
    public void testExceptionConstructor1() {
        String expResult = "Testing constructor 1";
        StockException se = new StockException(expResult);
        String result = se.getMessage();
        assertEquals(expResult, result);
    }
    
    public void testExceptionConstructor2() {
        Throwable expResult = new NullPointerException();
        StockException se = new StockException(expResult);
        Throwable result = se.getCause();
        assertEquals(expResult, result);
    }
    
    public void testExceptionConstructor3() {
        String expResult1 = "Testing constructor 3";
        Throwable expResult2 = new IOException();
        StockException se = new StockException(expResult1, expResult2);
        String result1 = se.getMessage();
        Throwable result2 = se.getCause();
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
    }
}

package com.mitchellbdunn.stockinfo.control.current;

import static junit.framework.Assert.assertEquals;
import junit.framework.TestCase;
import com.mitchellbdunn.stockinfo.model.current.CurrentStock;
import org.apache.http.client.utils.URIBuilder;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.webapp.WebAppContext;

/**
 *
 * @author Mitch
 */
public class TestCurrentStockFinder extends TestCase {
    
    private Server server;
    
    @Override
    public void setUp() throws Exception {
        server = new Server(1234);
        server.setStopAtShutdown(true);
        WebAppContext webAppContext = new WebAppContext();
        webAppContext.setContextPath("/test");
        webAppContext.setResourceBase("src/test/resources");
        webAppContext.setClassLoader(getClass().getClassLoader());
        server.addHandler(webAppContext);
        server.start();
    }
    
    public void testFindCurrentStock() throws Exception {
        CurrentStockFinder instance = new CurrentStockFinder();
        URIBuilder builder = new URIBuilder();
        builder.setPath("http://localhost:1234/test/yqlCurrentStock");
        instance.setBuilder(builder);
        
        CurrentStock stock = (CurrentStock)instance.findCurrentStock("TEST");
        assertEquals("GOOG", stock.getSymbol());
        assertEquals(905.00, stock.getAsk());
        assertEquals(928, (int)stock.getAverageDailyVolume());
        assertEquals(904.40, stock.getBid());
        assertEquals(-1.57, stock.getChange());
        assertNull(stock.getDividend());
        assertEquals(899.20, stock.getDayLow());
        assertEquals(905.525, stock.getDayHigh());
        assertEquals(636.0, stock.getYearLow());
        assertEquals(928.0, stock.getYearHigh());
        assertEquals(905.0, stock.getPrice());
        assertEquals(26.23, stock.getPeRatio());
        assertEquals(1.43, stock.getPegRatio());
        assertEquals("Google Inc.", stock.getCompanyName());
        assertEquals(1309821, (long)stock.getVolume());
        assertEquals("NasdaqNM", stock.getExchange());
    }
    
    public void testFindCurrentStock_BadSymbol() throws Exception {
        CurrentStockFinder instance = new CurrentStockFinder();
        URIBuilder builder = new URIBuilder();
        builder.setPath("http://localhost:1234/test/yqlCurrentStockException");
        instance.setBuilder(builder);
        
        CurrentStock stock = (CurrentStock)instance.findCurrentStock("TEST");
        assertNull(stock);
    }
    
    @Override
    public void tearDown() throws Exception {
        server.stop();
    }
}

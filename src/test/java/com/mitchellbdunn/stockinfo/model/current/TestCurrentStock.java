package com.mitchellbdunn.stockinfo.model.current;

import static junit.framework.Assert.assertEquals;
import junit.framework.TestCase;

/**
 *
 * @author Mitch
 */
public class TestCurrentStock extends TestCase {
    
    public void testVolume() {
        CurrentStock instance = new CurrentStock();
        Long expResult = 1234567890L;
        instance.setVolume(expResult);
        Long result = instance.getVolume();
        assertEquals(expResult, result);
    }
    
    public void testChange() {
        CurrentStock instance = new CurrentStock();
        Double expResult = 57.1;
        instance.setChange(expResult);
        Double result = instance.getChange();
        assertEquals(expResult, result);
    }
    
    public void testDayLow() {
        CurrentStock instance = new CurrentStock();
        Double expResult = 3.87;
        instance.setDayLow(expResult);
        Double result = instance.getDayLow();
        assertEquals(expResult, result);
    }
    
    public void testDayHigh() {
        CurrentStock instance = new CurrentStock();
        Double expResult = 26.62;
        instance.setDayHigh(expResult);
        Double result = instance.getDayHigh();
        assertEquals(expResult, result);
    }
    
    public void testPrice() {
        CurrentStock instance = new CurrentStock();
        Double expResult = 48.6;
        instance.setPrice(expResult);
        Double result = instance.getPrice();
        assertEquals(expResult, result);
    }
}

package com.mitchellbdunn.stockinfo.model;

import junit.framework.TestCase;
import org.joda.time.DateTime;

/**
 *
 * @author Mitch
 */
public class TestDividend extends TestCase {
    
    public void testDividendShare() {
        Dividend instance = new Dividend();
        Double expResult = 5.1;
        instance.setDividendShare(expResult);
        Double result = instance.getDividendShare();
        assertEquals(expResult, result);
    }

    public void testExDividendDate() {
        Dividend instance = new Dividend();
        DateTime expResult = new DateTime(1, 1, 1, 1, 1);
        instance.setExDividendDate(expResult);
        DateTime result = instance.getExDividendDate();
        assertEquals(expResult, result);
    }

    public void testDividendPayDate() {
        Dividend instance = new Dividend();
        DateTime expResult = new DateTime(2, 2, 2, 2, 2);
        instance.setDividendPayDate(expResult);
        DateTime result = instance.getDividendPayDate();
        assertEquals(expResult, result);
    }

    public void testDividendYield() {
        Dividend instance = new Dividend();
        Double expResult = 3.3;
        instance.setDividendYield(expResult);
        Double result = instance.getDividendYield();
        assertEquals(expResult, result);
    }
}

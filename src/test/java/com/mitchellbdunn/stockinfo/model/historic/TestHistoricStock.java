package com.mitchellbdunn.stockinfo.model.historic;

import java.util.ArrayList;
import java.util.List;
import junit.framework.TestCase;

/**
 *
 * @author Mitch
 */
public class TestHistoricStock extends TestCase {
    
    public void testPriceHistory() {
        HistoricStock instance = new HistoricStock();
        List<PriceHistory> expResult = new ArrayList<PriceHistory>();
        expResult.add(new PriceHistory());
        instance.setPriceHistory(expResult);
        List<PriceHistory> result = instance.getPriceHistory();
        assertEquals(expResult, result);
        assertEquals(1, result.size());
    }
}

package com.mitchellbdunn.stockinfo.model.historic;

import junit.framework.TestCase;
import org.joda.time.DateTime;

/**
 *
 * @author Mitch
 */
public class TestPriceHistory extends TestCase {
    
    public void testOpen() {
        PriceHistory instance = new PriceHistory();
        Double expResult = 1.4;
        instance.setOpen(expResult);
        Double result = instance.getOpen();
        assertEquals(expResult, result);
    }
    
    public void testClose() {
        PriceHistory instance = new PriceHistory();
        Double expResult = 2.5;
        instance.setClose(expResult);
        Double result = instance.getClose();
        assertEquals(expResult, result);
    }
    
    public void testAdjustedClose() {
        PriceHistory instance = new PriceHistory();
        Double expResult = 76.3;
        instance.setAdjustedClose(expResult);
        Double result = instance.getAdjustedClose();
        assertEquals(expResult, result);
    }
    
    public void testDayHigh() {
        PriceHistory instance = new PriceHistory();
        Double expResult = 38.5;
        instance.setDayHigh(expResult);
        Double result = instance.getDayHigh();
        assertEquals(expResult, result);
    }
    
    public void testDayLow() {
        PriceHistory instance = new PriceHistory();
        Double expResult = 9.44;
        instance.setDayLow(expResult);
        Double result = instance.getDayLow();
        assertEquals(expResult, result);
    }
    
    public void testDate() {
        PriceHistory instance = new PriceHistory();
        DateTime expResult = new DateTime(1, 2, 3, 4, 5);
        instance.setDate(expResult);
        DateTime result = instance.getDate();
        assertEquals(expResult, result);
    }
}

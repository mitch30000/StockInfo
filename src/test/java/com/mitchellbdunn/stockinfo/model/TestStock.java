package com.mitchellbdunn.stockinfo.model;

import static junit.framework.Assert.assertEquals;
import junit.framework.TestCase;

/**
 *
 * @author Mitch
 */
public class TestStock extends TestCase {
    
    public void testSymbol() {
        Stock instance = new Stock();
        String expResult = "symbol";
        instance.setSymbol(expResult);
        String result = instance.getSymbol();
        assertEquals(expResult, result);
    }

    public void testCompanyName() {
        Stock instance = new Stock();
        String expResult = "companyName";
        instance.setCompanyName(expResult);
        String result = instance.getCompanyName();
        assertEquals(expResult, result);
    }

    public void testExchange() {
        Stock instance = new Stock();
        String expResult = "exchange";
        instance.setExchange(expResult);
        String result = instance.getExchange();
        assertEquals(expResult, result);
    }
}
